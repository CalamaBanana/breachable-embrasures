# Breachable Embrasures

Forces enemy pawns to target embrasures of supported mods instead of wandering around like headless chickens. 

See the steam workshop description for the sales pitch.
https://steamcommunity.com/sharedfiles/filedetails/?id=2569778304

https://gfycat.com/equalloathsomehare **Without my mod**, notice how the gold-stockpile honey pots seem far more important than my pawns behind embrasures. A pure shooting gallery for my pawns.       

https://gfycat.com/unconsciousbittercaecilian **With my mod**. No mercy for embrasures. Do notice the longbowmen making use of their full range, too. 
          

**What it does:**

**Short version:** Melee pawns go for embrasure-rooms with pawns in them. Ranged pawns will make use of their "go into shooting position" logic if they detect enemies behind embrasures. If there are no pawns behind embrasures, the AI will default to their normal vanilla behaviour. 


**Long version:** The Rimworld AI works by using "Duty" lists of "JobDrivers" to assign "Jobs". If the first "JobDriver" on the list can not provide a valid "Job", the second one will be checked, and so on, until a valid "Job" is found. For example a pawn with the Assault_Colony duty checks if they can get a valid "Job" to take their stimulants, then a "Job" to assault nearby pawns they can reach, then a "Job" to smash stuff randomly in close range, then the same for long range. My code executes at the beginning of each of those smash JobGivers, and checks if it can assign a valid embrasure-breach job first, if not it defaults to the vanilla behaviour.

Rimworld 1.3 introduced a feature/bug, that leads to embrasures not dividing rooms anymore. This means, by code definition, your fortification is in the same "room" as the outside world. My code now checks if the assaulting pawn is in the same room as his target colonist, and if he can not reach that colonist, he will use vanilla sapper pathfinding to make himself a path.

If my code does not find a colonist hiding behind embrasures, the vanilla code continues with their normal behaviour. Which at that point is trying to assign any random thing to smash. 