﻿using HarmonyLib;
using LudeonTK;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace Breachable_Embrasures
{

    [StaticConstructorOnStartup]
    public static class BEUtilities
    {

        [TweakValue("Breachable Embrasure:")]
        private static bool ShowTargets = false;

        private static readonly IntRange TrashJobCheckOverrideInterval = new IntRange(450, 500);

        static BEUtilities()
        {


            //Log.Message("Patching Harmony");

            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "[O21] Seamless Embrasures"))
            //{
            //    supportedEmbrasures.Add("O21_Embrasure_Hole");
            //    supportedEmbrasures.Add("O21_Embrasure_Letterbox");
            //    Log.Message("Breachable Embrasures: [O21] Seamless Embrasures found");
            //}

            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Fortifications - Neolithic"))
            //{
            //    supportedEmbrasures.Add("FT_Palisade_Embrasures");
            //    Log.Message("Breachable Embrasures: Fortifications - Neolithic found");
            //}


            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Fortifications - Medieval"))
            //{
            //    supportedEmbrasures.Add("FT_CastleWallEmbrasures");
            //    Log.Message("Breachable Embrasures: Fortifications - Medieval found");
            //}

            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Combat Extended"))
            //{
            //    supportedEmbrasures.Add("Embrasure");
            //    Log.Message("Breachable Embrasures: Combat Extended found");
            //}
            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "[RF] Concrete (Continued)"))
            //{
            //    supportedEmbrasures.Add("Embrasure");
            //    supportedEmbrasures.Add("BrickEmbrasure");
            //    supportedEmbrasures.Add("RFFConcreteEmbrasure");
            //    supportedEmbrasures.Add("RFFPlasticreteEmbrasure");
            //    Log.Message("Breachable Embrasures: [RF] Concrete (Continued) found");
            //}
            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "[HRK] VFE Spacer Module - Reinforced Embrasures"))
            //{
            //    supportedEmbrasures.Add("HRK_ReinforcedEmbrasure");
            //    Log.Message("Breachable Embrasures: [HRK] VFE Spacer Module - Reinforced Embrasures found");
            //}
            //if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Reinforced Walls"))
            //{
            //    supportedEmbrasures.Add("NEC_ReinforcedEmbrasure");
            //    Log.Message("Breachable Embrasures: Reinforced Walls found");
            //}



            //if (!supportedEmbrasures.Any())
            //{
            //    Log.Warning("Breachable Embrasures: No supported embrasures found.");
            //}
            //else
            //{
            //    Harmony harmony = new Harmony("BreachableEmbrasures");
            //    harmony.PatchAll();
            //}

            //foreach (string s in supportedEmbrasures)
            //{
            //    Log.Message("BE: "+s+" supported");
            //}


            Harmony harmony = new Harmony("BreachableEmbrasures");
            harmony.PatchAll();

        }


        public static List<string> supportedEmbrasures = new List<string>();


        public static bool isSupportedEmbrasure(Building embrasure)
        {
            if (supportedEmbrasures.Any(s => string.Equals(embrasure.def.defName, s)))
            {
                //Log.Message(embrasure.def.defName + " is supported");
                return true;
            }
            return false;
        }

        public static bool isSupportedStructure(Building embrasure)
        {
            Thing thing = embrasure;
            if (thing.def.IsDoor)
            {
                return true;
            }
            return false;
        }


        public static bool NoRandomShouldTrashBuilding(Pawn pawn, Building b, bool attackAllInert = false)
        {
            //Log.Message("Stop 1");
            if (!debugShouldTrashBuilding(b))
            {
                //Log.Message("Spot 1");
                return false;
            }

            if (pawn.mindState.spawnedByInfestationThingComp && b.GetComp<CompCreatesInfestations>() != null)
            {
                //Log.Message("Spot 2");
                return false;
            }


            //No chance, only hate. We are pulling a random embrasure already
            //if (((b.def.building.isInert || b.def.IsFrame) && !attackAllInert) || b.def.building.isTrap)
            //{
            //    Log.Message("Stop 3.5");
            //    int num = GenLocalDate.HourOfDay(pawn) / 3;
            //    int specialSeed = b.GetHashCode() * 612361 ^ pawn.GetHashCode() * 391 ^ num * 73427324;
            //    if (!Rand.ChanceSeeded(0.008f, specialSeed))
            //    {
            //        return false;
            //    }
            //}


            return debugCanTrash(pawn, b) && pawn.HostileTo(b);
        }


        public static bool debugShouldTrashBuilding(Building b)
        {
            if (((b != null) ? b.def.building : null) == null)
            {
                //Log.Message("Spot 3");
                return false;
            }
            if (!b.def.useHitPoints || b.def.building.ai_neverTrashThis)
            {
                //Log.Message("Spot 4");
                return false;
            }
            if (b.def.building.isTrap)
            {
                //Log.Message("Spot 5");
                return false;
            }
            CompCanBeDormant comp = b.GetComp<CompCanBeDormant>();
            //Log.Message("Spot 6 "+ ((comp == null || comp.Awake) && (b.Faction == null || b.Faction != Faction.OfMechanoids)));
            return (comp == null || comp.Awake) && (b.Faction == null || b.Faction != Faction.OfMechanoids);
        }
        private static bool debugCanTrash(Pawn pawn, Thing t)
        {
            //Log.Message("Stop 7"+ (pawn.CanReach(t, PathEndMode.Touch, Danger.Some, false, false, TraverseMode.ByPawn) && !t.IsBurning()));
            return pawn.CanReach(t, PathEndMode.Touch, Danger.Some, false, false, TraverseMode.ByPawn) && !t.IsBurning();
        }







        public static Job shootThroughEmbrasures(Pawn pawn)
        {
            /*
             * 
             * RANGED HANDLING
             * This first block handles ranged enemies. Instead of smashing randomly, they will search a shooting position and shoot ito your embrasures      
             * Code is an stitched together from JobGiverAI_Enemy and related methods.
            */


            //Log.Message(pawn.Name.ToString() + " is ranged");
            IntVec3 intVec = pawn.mindState.duty.focus.Cell;

            if (intVec.IsValid && (float)intVec.DistanceToSquared(pawn.Position) < 100f && intVec.GetRoom(pawn.Map) == pawn.GetRoom(RegionType.Set_All) && intVec.WithinRegions(pawn.Position, pawn.Map, 9, TraverseMode.NoPassClosedDoors, RegionType.Set_Passable))
            {
                pawn.GetLord().Notify_ReachedDutyLocation(pawn);
                return null;
            }

            IAttackTarget attackTarget;

            if (!intVec.IsValid)
            {
                if (!(from x in pawn.Map.attackTargetsCache.GetPotentialTargetsFor(pawn)
                      where !x.ThreatDisabled(pawn) && x.Thing.Faction == Faction.OfPlayer && pawn.CanReach(x.Thing, PathEndMode.OnCell, Danger.Deadly, false, false, TraverseMode.PassAllDestroyableThings)
                      select x).TryRandomElement(out attackTarget))
                {
                    return null;
                }
                intVec = attackTarget.Thing.Position;
            }
            Pawn targetPawn = intVec.GetFirstPawn(pawn.Map);
            bool allowManualCastWeapons = !pawn.IsColonist;
            IntVec3 dest;

            if (targetPawn == null)
            {
                return null;
            }

            Verb verb = pawn.TryGetAttackVerb(targetPawn, allowManualCastWeapons);
            if (verb == null)
            {
                return null;
            }

            CastPositionRequest cpR = new CastPositionRequest
            {
                caster = pawn,
                target = targetPawn,
                verb = verb,
                maxRangeFromTarget = verb.verbProps.range,
                wantCoverFromTarget = (verb.verbProps.range > 5f)
            };

            if (CastPositionFinder.TryFindCastPosition(cpR, out dest))
            {
                //Log.Message(dest+ " is his destination to shoot");

                if (ShowTargets)
                {
                    Find.CurrentMap.debugDrawer.FlashCell(dest, 1f, "dest", 100);
                }

                if (dest == pawn.Position)
                {
                    return JobMaker.MakeJob(JobDefOf.Wait_Combat, JobGiver_AIFightEnemy.ExpiryInterval_ShooterSucceeded.RandomInRange, true);
                }


                return JobMaker.MakeJob(JobDefOf.Goto, dest); ;
            }


            return null;
        }





        public static Job NoFireTrashJob(Pawn pawn, Thing t, bool allowPunchingInert = false, bool killIncappedTarget = false)
        {
            if (t is Plant)
            {
                Job job = JobMaker.MakeJob(JobDefOf.Ignite, t);
                FinalizeTrashJob(job);
                return job;
            }
            if (pawn.equipment != null && Rand.Value < 0.7f)
            {
                foreach (Verb verb in pawn.equipment.AllEquipmentVerbs)
                {
                    if (verb.verbProps.ai_IsBuildingDestroyer)
                    {
                        Job job2 = JobMaker.MakeJob(JobDefOf.UseVerbOnThing, t);
                        job2.verbToUse = verb;
                        FinalizeTrashJob(job2);
                        return job2;
                    }
                }
            }
            Job job3;

            Building building = t as Building;
            if (building != null && building.def.building.isInert && !allowPunchingInert)
            {
                return null;
            }
            job3 = JobMaker.MakeJob(JobDefOf.AttackMelee, t);

            job3.killIncappedTarget = killIncappedTarget;
            FinalizeTrashJob(job3);
            return job3;
        }

        
        private static void FinalizeTrashJob(Job job)
        {
            job.expiryInterval = TrashJobCheckOverrideInterval.RandomInRange;
            job.checkOverrideOnExpire = true;
            job.expireRequiresEnemiesNearby = true;
        }









        public static List<Building> reachableEmbrasures(Pawn pawn)
        {

            IntVec3 intVec = pawn.mindState.duty.focus.Cell;
            if (intVec.IsValid && (float)intVec.DistanceToSquared(pawn.Position) < 100f && intVec.GetRoom(pawn.Map) == pawn.GetRoom(RegionType.Set_All) && intVec.WithinRegions(pawn.Position, pawn.Map, 9, TraverseMode.NoPassClosedDoors, RegionType.Set_Passable))
            {
                pawn.GetLord().Notify_ReachedDutyLocation(pawn);
                return null;
            }

            IAttackTarget attackTarget;

            if (!intVec.IsValid)
            {
                if (!(from x in pawn.Map.attackTargetsCache.GetPotentialTargetsFor(pawn)
                      where !x.ThreatDisabled(pawn) && x.Thing.Faction == Faction.OfPlayer && pawn.CanReach(x.Thing, PathEndMode.OnCell, Danger.Deadly, false, false, TraverseMode.PassAllDestroyableThings)
                      select x).TryRandomElement(out attackTarget))
                {
                    return null;
                }
                intVec = attackTarget.Thing.Position;
            }

            Pawn targetPawn = intVec.GetFirstPawn(pawn.Map);
            if (targetPawn != null)
            {                


                CellRect cellRect = CellRect.CenteredOn(intVec, 5);

                //Log.Message("my target" + intVec);
                //Log.Message("my target" + intVec.GetFirstPawn(pawn.Map).Name.ToString());

                //Find.CurrentMap.debugDrawer.FlashCell(intVec, 1f, "ME", 50);



                //The ones our colonists can reach. Nasty check for adjacent tiles, as PathEndMode.Touch returns unwalkable corner pieces
                List<(Building, int)> embrasuresNearColonists = new List<(Building, int)>();
                foreach (IntVec3 c in cellRect.Cells.Where<IntVec3>(c => c.InBounds(pawn.Map)))
                {
                    Building embrasure = c.GetEdifice(pawn.Map);
                    if (embrasure != null
                        && BEUtilities.isSupportedEmbrasure(embrasure)
                        && GenAdj.CellsAdjacentCardinal(embrasure).Any(cell => targetPawn.CanReach(cell, PathEndMode.OnCell, Danger.Some, false, false, TraverseMode.NoPassClosedDoors))
                        //&& targetPawn.CanReach(embrasure, PathEndMode.Touch, Danger.Deadly, false, false, TraverseMode.NoPassClosedDoors)                        
                        )
                    {
                        //Log.Message(pawn.Name.ToString() + " Found " + embrasure.def.defName);
                        //Find.CurrentMap.debugDrawer.FlashCell(embrasure.Position, 1f, "REACH", 50);
                        embrasuresNearColonists.Add((embrasure, 0));
                    }
                }

                //Getting all embrasures and doors connected to the initial list and assigning a priority number
                int embrasureCount = embrasuresNearColonists.Count;
                for (int i = 0; i < embrasureCount; i++)
                {
                    Building embrasure = embrasuresNearColonists[i].Item1;

                    foreach (IntVec3 c in GenAdj.CellsAdjacentCardinal(embrasure).Where<IntVec3>(c
                        => c.InBounds(pawn.Map)
                        && c.GetEdifice(pawn.Map) != null
                        && (BEUtilities.isSupportedEmbrasure(c.GetEdifice(pawn.Map)) || BEUtilities.isSupportedStructure(c.GetEdifice(pawn.Map)))
                        && !embrasuresNearColonists.Any(e => e.Item1.Position == c)
                        ))
                    {
                        embrasuresNearColonists.Add((c.GetEdifice(pawn.Map), embrasuresNearColonists[i].Item2 + 1)); //Adding with a lower priority than parent
                        embrasureCount++;
                        //Find.CurrentMap.debugDrawer.FlashCell(c, 1f, ""+embrasuresNearColonists.Last().Item2, 50);
                    }
                }



                //Only those reachable by the attacking pawn
                //IEnumerable<(Building, int)> helper = embrasuresNearColonists.Where(embrasure => pawn.CanReach(embrasure.Item1, PathEndMode.Touch, Danger.Some, false, false, TraverseMode.ByPawn));

                //Removing those that can not be reached from cardinal directions by an attacking pawns. This avoids them cutting diagonally into corners they can not walk in. 
                IEnumerable<(Building, int)> helper = embrasuresNearColonists.Where(embrasure => GenAdj.CellsAdjacentCardinal(embrasure.Item1).Any(cell => pawn.CanReach(cell, PathEndMode.OnCell, Danger.Some, false, false, TraverseMode.ByPawn)));

                //Now we only keep those with the lowest priority
                //Log.Message("Here its already " + helper.Count());
                IEnumerable<(Building, int)> resultIE = helper.Where(embrasure => embrasure.Item2 == helper.Min(t => t.Item2));
                //Log.Message("Here its " + resultIE.Count());
                List<Building> resultList = new List<Building>();
                foreach ((Building, int) entry in resultIE)
                {
                    if (ShowTargets)
                    {
                        Find.CurrentMap.debugDrawer.FlashCell(entry.Item1.Position, 1f, "" + entry.Item2, 100);
                    }
                    resultList.Add(entry.Item1);
                }
                //Log.Message("Returning " + resultList.Count);

                return resultList;
            }

            return null;

        }

        


        public static Job sapEmbrasures(Pawn pawn)
        {

            IntVec3 intVec = pawn.mindState.duty.focus.Cell;
            if (intVec.IsValid && (float)intVec.DistanceToSquared(pawn.Position) < 100f && intVec.GetRoom(pawn.Map) == pawn.GetRoom(RegionType.Set_All) && intVec.WithinRegions(pawn.Position, pawn.Map, 9, TraverseMode.NoPassClosedDoors, RegionType.Set_Passable))
            {
                pawn.GetLord().Notify_ReachedDutyLocation(pawn);
                return null;
            }

            IAttackTarget attackTarget;

            if (!intVec.IsValid)
            {
                if (!(from x in pawn.Map.attackTargetsCache.GetPotentialTargetsFor(pawn)
                      where !x.ThreatDisabled(pawn) && x.Thing.Faction == Faction.OfPlayer && pawn.CanReach(x.Thing, PathEndMode.OnCell, Danger.Deadly, false, false, TraverseMode.PassAllDestroyableThings)
                      select x).TryRandomElement(out attackTarget))
                {
                    return null;
                }
                intVec = attackTarget.Thing.Position;
            }
            Pawn targetPawn = intVec.GetFirstPawn(pawn.Map);
            
            //Log.Message("Target pawn "+ targetPawn.Name.ToStringShort);


            if ((targetPawn != null) && (pawn.GetRoom() == targetPawn.GetRoom()))
            {
                if(!pawn.CanReach(targetPawn, PathEndMode.OnCell, Danger.Some, false, false, TraverseMode.NoPassClosedDoors))
                {
                    //Log.Message("Stop 2");
                    using (PawnPath pawnPath = pawn.Map.pathFinder.FindPath(pawn.Position, intVec, TraverseParms.For(pawn, Danger.Deadly, TraverseMode.PassAllDestroyableThings, false, false, false), PathEndMode.OnCell, null))
                    {
                        IntVec3 cellBeforeBlocker;
                        Thing thing = pawnPath.FirstBlockingBuilding(out cellBeforeBlocker, pawn);
                        if (thing != null)
                        {
                            //Log.Message("Stop 3");
                            Job job = DigUtility.PassBlockerJob(pawn, thing, cellBeforeBlocker, true, false);
                            if (job != null)
                            {
                                //Log.Message("Stop 4");
                                if (ShowTargets)
                                {
                                    Find.CurrentMap.debugDrawer.FlashCell(thing.Position, 1f, "BR", 50);
                                }                                 
                                return job;
                            }
                        }
                    }
                }
                
            }            
            return null;
        }



    }






}


