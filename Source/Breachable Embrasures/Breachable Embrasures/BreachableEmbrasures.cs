﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace Breachable_Embrasures
{
   





    [HarmonyPatch(typeof(JobGiver_AITrashColonyClose), "TryGiveJob")]
    public class BE_AITrashColonyClose
    {

        public static bool Prefix(ref Job __result, Pawn pawn)
        {

 

               
            if (!pawn.HostileTo(Faction.OfPlayer))
            {
                //return;
                return true;
            }


            //Handling of ranged pawns, they now always try to get into shooting positions
            IAttackTargetSearcher attackTargetSearcher = pawn;
            if (attackTargetSearcher.CurrentEffectiveVerb != null && !attackTargetSearcher.CurrentEffectiveVerb.verbProps.IsMeleeAttack)
            {
                Job rangedJob = BEUtilities.shootThroughEmbrasures(pawn);
                if (rangedJob != null)
                {
                    __result = rangedJob;
                    return false;
                }

                return true;
            }





            ////Handling of melee pawns,

            Job meleeJob = BEUtilities.sapEmbrasures(pawn);
            if (meleeJob != null)
            {
                __result = meleeJob;
                return false;
            }


        


            ////Handling of melee pawns, makes a list of valid embrasures in 5-tile radius box around the pawn, then either picks the most damaged one or a random one
            //CellRect cellRect = CellRect.CenteredOn(pawn.Position, 5);
            ////Find.CurrentMap.debugDrawer.FlashCell(pawn.Position, 1f, "ME", 50);

            //List<Building> reachAbleEmbrasures = BEUtilities.reachableEmbrasures(pawn);
            //if(reachAbleEmbrasures != null)
            //{
            //    //Log.Message("I spot " + reachAbleEmbrasures.Count);
            //    reachAbleEmbrasures.RemoveAll(x => !cellRect.Contains(x.Position));
            //    //Log.Message("I reach " + reachAbleEmbrasures.Count);

            //    if (reachAbleEmbrasures.Any())
            //    {
            //        Building embrasure = reachAbleEmbrasures.OrderBy(x => x.HitPoints).First();

            //        if (embrasure.HitPoints > (embrasure.MaxHitPoints / 2))
            //        {
            //            //Log.Message(pawn.Name.ToString() + " Found " + embrasure.def.defName + " with " + embrasure.HitPoints/embrasure.MaxHitPoints + "Too high, so we random");
            //            embrasure = reachAbleEmbrasures.RandomElement<Building>();
            //        }

            //        //Log.Message(pawn.Name.ToString() + " Found " + embrasure.def.defName + " with " + embrasure.HitPoints);
            //        //Find.CurrentMap.debugDrawer.FlashCell(embrasure.Position, 1f, ""+embrasure.HitPoints, 50);

            //        if (BEUtilities.NoRandomShouldTrashBuilding(pawn, embrasure, false))
            //        {
            //            Job job = BEUtilities.NoFireTrashJob(pawn, embrasure, true, false);
            //            if (job != null)
            //            {
            //                //Log.Message("Not null");
            //                __result = job;
            //                return false;
            //                //return;
            //            }
            //        }

            //    }
            //}




            //return;
            return true;

        }

    }


    [HarmonyPatch(typeof(JobGiver_AITrashBuildingsDistant), "TryGiveJob")]
    public class BE_AITrashBuildingsDistant
    {


        public static bool Prefix(ref Job __result, JobGiver_AITrashBuildingsDistant __instance, Pawn pawn)
        {



            ////Getting a list of reachable embrasure targets
            //List<Building> reachableEmbrasures = BEUtilities.reachableEmbrasures(pawn);
            //if (reachableEmbrasures == null || reachableEmbrasures.Count() == 0)
            //{
            //    //Log.Message("NO EMBRASURES?!");
            //    return true;
            //}

            //Handling of ranged pawns, they now always try to get into shooting positions
            IAttackTargetSearcher attackTargetSearcher = pawn;
            if (attackTargetSearcher.CurrentEffectiveVerb != null && !attackTargetSearcher.CurrentEffectiveVerb.verbProps.IsMeleeAttack)
            {
                Job rangedJob = BEUtilities.shootThroughEmbrasures(pawn);
                if(rangedJob != null)
                {
                    __result = rangedJob;
                    return false;
                }

                return true;
            }


            //Handling of melee pawns.



            Job meleeJob = BEUtilities.sapEmbrasures(pawn);
            if (meleeJob != null)
            {
                __result = meleeJob;
                return false;
            }



            //Building building = reachableEmbrasures.RandomElement<Building>();

            //if (BEUtilities.NoRandomShouldTrashBuilding(pawn, building, __instance.attackAllInert))
            //{
            //    Job job = BEUtilities.NoFireTrashJob(pawn, building, true, false);

            //    if (job != null)
            //    {

            //        __result = job;
            //        return false;
            //    }
            //}

            return true;
        }



    }





}
